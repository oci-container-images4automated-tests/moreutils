defmodule Buildah.Pacman.Moreutils do

    alias Buildah.{Pacman, Pacman.Catatonit, Cmd}

    def on_container(container, options) do
        Pacman.packages_no_cache(container, [
            "moreutils"
        ], options)
        {_, 0} = Cmd.run(container, ["sh", "-c",
            "command -v chronic"])
        {_, 0} = Cmd.config(
            container,
            env: ["CHRONIC=chronic"],
            into: IO.stream(:stdio, :line)
        )
    end

    def test(container, image_ID, options) do
        {_, 0} = Cmd.run(container, ["sh", "-c",
            "command -v chronic"], into: IO.stream(:stdio, :line))
        {_, 0} = Podman.Cmd.run(image_ID, [
            "printenv", "CHRONIC"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )
        {_, 0} = Podman.Cmd.run(image_ID, ["sh", "-c",
            "command -v $CHRONIC"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )
    end

end

